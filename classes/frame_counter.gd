class_name FrameCounter

var frames_per_second: int
var num_frames: int setget _set_num_frames
var num_seconds: float setget _set_num_seconds

signal counter_changed(new_num_frames, new_num_seconds)

func _init(frames_per_second):
	self.frames_per_second = frames_per_second
	self.num_frames = 0

func increment():
	self.num_frames += 1

func print_state():
	print("FrameCounter::num_frames: %d" % self.num_frames)
	print("FrameCounter::num_seconds: %f" % self.num_seconds)

func _set_num_seconds(value):
	num_seconds = value
	num_frames = num_seconds * frames_per_second
	emit_signal("counter_changed", num_frames, num_seconds)

func _set_num_frames(value):
	num_frames = value
	num_seconds = (1.0 * num_frames / frames_per_second)
	emit_signal("counter_changed", num_frames, num_seconds)
