class_name FrameTimer

var _frame_counter: FrameCounter
var _time_delay: float
var _start_time: float = -1
var _elapsed_time: float = -1

signal timer_complete()

func _init(counter: FrameCounter):
	self._frame_counter = counter

func start(time_delay: float):
	self._time_delay = time_delay
	self._frame_counter.connect("counter_changed", self, "_frame_counter_on_counter_changed")
	self._start_time = self._frame_counter.num_seconds
	self._elapsed_time = 0

func stop():
	self._frame_counter.disconnect("counter_changed", self, "_frame_counter_on_counter_changed")

func _frame_counter_on_counter_changed(new_num_frames, new_num_seconds):
	self._elapsed_time = new_num_seconds - self._start_time
	
	if self._elapsed_time >= self._time_delay:
		stop()
		emit_signal("timer_complete")
